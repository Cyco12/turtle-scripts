-- Phillip D 5/26/2023
-- A basestation program for automatically sending a redstone signal based on a modem message.
--  with speaker & modem support

REDSTONE_OUTPUT = 'right'

_modem = peripheral.find("modem") or error("No modem attached. Transmit functionality will not work", 0)
_speaker = peripheral.find("speaker") or error("No speaker attached. Sound functionality will not work")
_modem.open(13761)

function openRoof()
    _speaker.playSound("infinitybuttons:alarm")
    rs.setOutput(REDSTONE_OUTPUT, true)
    sleep(0.5)
    rs.setOutput(REDSTONE_OUTPUT, false)
end

function listenModem()
    while true do
        local event, side, channel, replyChannel, message, distance = os.pullEvent("modem_message")
        _message = tostring(message)
        if _message ~= nil then
            if _message == 'OPEN_ROOF' then
                openRoof()
            end
        end
        sleep(0.25)
    end
end
