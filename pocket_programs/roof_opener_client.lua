-- Phillip D 5/26/2023
-- The client/pocket program intended to pair with roof_opener_base.lua
-- Sends a modem message to the basestation to open the roof when within a configured GPS boundary

-- Requires a GPS constellation to be functional and accessible nearby this client

boundaries = {
    x = {-169, -53},
    y = {75, 140},
    z = {16, -80}
}

_modem = peripheral.find("modem") or error("No modem attached. Transmit functionality will not work", 0)
_modem.open(13761)

function openRoof()
    _modem.transmit(13761, 13761, "OPEN_ROOF")
end

function geofence()
    x, y, z = gps.locate()

    term.setCursorPos(5,5)
    print("location is: " .. x .. "x " .. y .. "y " .. z .. "z")
    if y < boundaries['y'][2] and y > boundaries['y'][1] and ((x < boundaries['x'][2] and x > boundaries['x'][1]) or (z < boundaries['x'][2] and z > boundaries['x'][1])) then
        openRoof()
    end
end

while true do
    sleep(0.2)
    geofence()
end

